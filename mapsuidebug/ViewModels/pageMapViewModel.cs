﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using Xamarin.Forms;

namespace mapsuidebug.ViewModels
{
    public class pageMapViewModel : INotifyPropertyChanged
    {
        public ICommand clickBackCommand;
        public ICommand ClickBackCommand
        {
            get { return clickBackCommand; }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public pageMapViewModel()
        {
            clickBackCommand = new Command(clickBack);

        }

        async void clickBack(object s)
        {
            Console.WriteLine("Clicked back");
            await Application.Current.MainPage.Navigation.PopModalAsync(false);
        }
    }
}
