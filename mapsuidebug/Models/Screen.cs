﻿using System;
using System.Collections.Generic;

namespace mapsuidebug.Models
{
    public class Screen
    {
       
        public int ScreenId { get; set; }
        public int ParentId { get; set; }
        public int ScreenTypeId { get; set; }
       
        public string ScreenTitle { get; set; }
       
        public string ScreenSubTitle { get; set; }
       
        public double MapPinLat { get; set; }
        public double MapPinLon { get; set; }
       
        public string MapPinType { get; set; }
        public List<ScreenContent> ScreenContentItems { get; set; }
     
        public List<ScreenParameter> ScreenParameters { get; set; }
    }
}
