﻿using System;
using System.Collections.Generic;

namespace mapsuidebug.Models
{
    public class ScreenContent
    {
      
        public int ScreenContentId { get; set; }
        public int ScreenId { get; set; }
     
        public string ContentType { get; set; }
        public int ContentOrder { get; set; }
      
        public string ContentSubType { get; set; }
        public int ContentSubTypeOrder { get; set; }
       
        public string Title { get; set; }
      
        public string SubTitle { get; set; }
     
        public string Picture { get; set; }
     
        public string ActionType { get; set; }
      
        public string ActionContent { get; set; }
     
        public string TextDescription { get; set; }
     
        public List<ScreenContentParameter> SubItemParameters { get; set; }
    }
}
