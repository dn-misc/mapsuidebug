﻿using System;
namespace mapsuidebug.Models
{
    public class ScreenParameter
    {
        public int ParameterId { get; set; }
        public int ScreenId { get; set; }
        public string ParameterName { get; set; }
        public string ParameterValue { get; set; }
    }
}
