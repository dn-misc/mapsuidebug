﻿using System;
using System.Collections.Generic;

namespace mapsuidebug.Models
{
    public class AppConfig
    {
        public float AppVersion { get; set; }
        public string CMSVersion { get; set; }
        public bool RefreshMediaFiles { get; set; }
        public List<Screen> AppScreens { get; set; }
    }
}
