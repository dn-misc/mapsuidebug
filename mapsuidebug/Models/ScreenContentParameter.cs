﻿using System;
namespace mapsuidebug.Models
{
    public class ScreenContentParameter
    {
     
        public int ParameterId { get; set; }
        public int ObjectId { get; set; }
      
        public string ParameterName { get; set; }
        public string ParameterValue { get; set; }
    }
}
