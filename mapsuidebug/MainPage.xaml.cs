﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using mapsuidebug.Models;
using mapsuidebug.ViewModels;
using Xamarin.Forms;
using System.IO;
using System.Xml.Linq;
using System.Xml.Serialization;
using Mapsui.Projection;
using Mapsui.Utilities;
using System.Reflection;
using Mapsui.UI.Forms;
using System.Collections.ObjectModel;
using Mapsui.Rendering.Skia;
using Xamarin.Essentials;
using System.Text.RegularExpressions;

namespace mapsuidebug
{
    public partial class MainPage : ContentPage
    {
        private DateTime lastClickedTime;
        CancellationTokenSource cts;
        double x, y;
        private XDocument mainXML;
        private List<LangScreen> appScreens;
        private List<LangScreen> mapScreens = new List<LangScreen>();

        public MainPage()
        {
            InitializeComponent();
            BindingContext = new pageMapViewModel();
            LoadXML();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

          
            ShowOnlinelineMap();
            Console.WriteLine("Show Track");

            //ShowMapTrack();
            Console.WriteLine("Show Pins");
            ShowMapPins();

            Console.WriteLine("Show Done");

            //mapView.Zoomed += MapZoomed();
            mapView.PinClicked += OnPinClicked;
            lastClickedTime = DateTime.Now;

            //update location timer
            Device.StartTimer(TimeSpan.FromSeconds(15), () =>
            {
                SetCurrentLocation();
                return true;
            });

            Mapsui.Logging.Logger.LogDelegate += (level, message, ex) => {
                Console.WriteLine("MAPSUI Error: " + message + " - " + ex);
            };
        }

        protected override void OnDisappearing()
        {
            if (cts != null && !cts.IsCancellationRequested)
                cts.Cancel();
            base.OnDisappearing();
        }

        public void ShowOnlinelineMap()
        {
            var map = new Mapsui.Map
            {
            };

            //use online layer
            var tileLayer = OpenStreetMap.CreateTileLayer();

            map.Layers.Add(tileLayer);

            var centerOfMap = new Mapsui.Geometries.Point(43.471811, 17.044151);
            // OSM uses spherical mercator coordinates. So transform the lon lat coordinates to spherical mercator
            var sphericalMercatorCoordinate = SphericalMercator.FromLonLat(centerOfMap.Y, centerOfMap.X); //This is Longitude + Latitude (inversed)
            map.Home = n => n.NavigateTo(sphericalMercatorCoordinate, map.Resolutions[13]);

            mapView.Map = map;
        }

        public async void ShowMapPins()
        {
            //load icons
            Assembly assembly = IntrospectionExtensions.GetTypeInfo(typeof(App)).Assembly;
            Stream embeddedPinStream = assembly.GetManifestResourceStream("mapsuidebug.pin_blue.png");
            var pin_blue = embeddedPinStream.ToBytes();
            embeddedPinStream = assembly.GetManifestResourceStream("mapsuidebug.pin_caffebar.png");
            var pin_caffebar = embeddedPinStream.ToBytes();
            embeddedPinStream = assembly.GetManifestResourceStream("mapsuidebug.pin_finance.png");
            var pin_finance = embeddedPinStream.ToBytes();
            embeddedPinStream = assembly.GetManifestResourceStream("mapsuidebug.pin_health.png");
            var pin_health = embeddedPinStream.ToBytes();
            embeddedPinStream = assembly.GetManifestResourceStream("mapsuidebug.pin_restaurant.png");
            var pin_restaurant = embeddedPinStream.ToBytes();
            embeddedPinStream = assembly.GetManifestResourceStream("mapsuidebug.pin_shopping.png");
            var pin_shopping = embeddedPinStream.ToBytes();
            embeddedPinStream = assembly.GetManifestResourceStream("mapsuidebug.pin_sight.png");
            var pin_sight = embeddedPinStream.ToBytes();

            //get pins from screens
            mapScreens = appScreens.Where(s => s.ScreenTypeId == 3 && (s.MapPinLat != 0 && s.MapPinLon != 0)).ToList();

            var list = new List<Pin>();
            foreach (LangScreen mapPinScreen in mapScreens)
            {
                Byte[] xPinIcon = null;
                if (mapPinScreen.MapPinType == "sight")
                    xPinIcon = pin_sight;
                else if (mapPinScreen.MapPinType == "restaurant")
                    xPinIcon = pin_restaurant;
                else if (mapPinScreen.MapPinType == "caffebar")
                    xPinIcon = pin_caffebar;
                else if (mapPinScreen.MapPinType == "shop")
                    xPinIcon = pin_shopping;
                else if (mapPinScreen.MapPinType == "pharmacy")
                    xPinIcon = pin_health;
                else if (mapPinScreen.MapPinType == "finance")
                    xPinIcon = pin_finance;
                else
                    xPinIcon = pin_blue;

                var pin = new Pin(mapView)
                {
                    Label = mapPinScreen.ScreenTitle,
                    Position = new Position(mapPinScreen.MapPinLat, mapPinScreen.MapPinLon),
                    Type = PinType.Icon,
                    Icon = xPinIcon,
                    //Type = PinType.Pin,
                    //Color = Xamarin.Forms.Color.Blue,
                    //Transparency = 0.2f,
                    //Scale = 1,

                };
                pin.Callout.Type = CalloutType.Single;
                pin.Callout.CalloutClicked += (s, e) =>
                {
                    if (e.NumOfTaps == 1)
                    {
                        var span = DateTime.Now - lastClickedTime;
                        if ((Int32)span.TotalMilliseconds > 1000)
                        {
                            var p = e.Callout.Pin;
                            //p.HideCallout();
                            Console.WriteLine("Tapped on callout for pin: " + p.Label);
                            p.HideCallout();
                        }
                        lastClickedTime = DateTime.Now;
                        e.Handled = true;
                        return;
                    }
                };


                list.Add(pin);
            }
            ((ObservableRangeCollection<Pin>)((MapView)mapView).Pins).AddRange(list);
        }

        public async void SetCurrentLocation()
        {
            //update to current location
            try
            {
                var request = new GeolocationRequest(GeolocationAccuracy.Medium, TimeSpan.FromSeconds(10));
                cts = new CancellationTokenSource();
                var location = await Geolocation.GetLastKnownLocationAsync();

                if (location != null)
                {
                    Console.WriteLine($"Latitude: {location.Latitude}, Longitude: {location.Longitude}, Altitude: {location.Altitude}");

                    mapView.MyLocationEnabled = true;
                    //mapView.MyLocationFollow = true;
                    mapView.MyLocationLayer.UpdateMyLocation(new Mapsui.UI.Forms.Position(location.Latitude, location.Longitude), true);

                    mapView.Refresh();
                }
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                // Handle not supported on device exception
            }
            catch (FeatureNotEnabledException fneEx)
            {
                // Handle not enabled on device exception
            }
            catch (PermissionException pEx)
            {
                // Handle permission exception
            }
            catch (Exception ex)
            {
                // Unable to get location
            }
        }

        void Handle_POISelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (((ListView)sender).SelectedItem == null)
                return;

            LangScreen POISelected = (LangScreen)e.SelectedItem;

            //focus on POI
            var centerOfMap = new Mapsui.Geometries.Point(POISelected.MapPinLat, POISelected.MapPinLon);
            // OSM uses spherical mercator coordinates. So transform the lon lat coordinates to spherical mercator
            var sphericalMercatorCoordinate = SphericalMercator.FromLonLat(centerOfMap.Y, centerOfMap.X); //This is Longitude + Latitude (inversed)
            if (switchOfflineMap.IsToggled)
                mapView.Navigator.NavigateTo(sphericalMercatorCoordinate, mapView.Map.Resolutions[3]);
            else
                mapView.Navigator.NavigateTo(sphericalMercatorCoordinate, mapView.Map.Resolutions[13]);

            //show pin
            foreach (Pin xPin in mapView.Pins)
            {
                xPin.HideCallout();
            }

            Pin currPin = ((ObservableRangeCollection<Pin>)((MapView)mapView).Pins).Where(p => p.Label == POISelected.ScreenTitle).FirstOrDefault();
            currPin.ShowCallout();
        }


        void SearchBar_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (e.NewTextValue != string.Empty && e.NewTextValue != null)
            {

                var filteredMapScreens = new List<LangScreen>();
                foreach (LangScreen mapScreenItem in mapScreens)
                {
                    //if (mapScreenItem.ScreenTitle.Contains(e.NewTextValue))
                    if (Regex.IsMatch(mapScreenItem.ScreenTitle, Regex.Escape(e.NewTextValue), RegexOptions.IgnoreCase))
                    {
                        filteredMapScreens.Add(mapScreenItem);
                    }
                }
                POIListView.ItemsSource = filteredMapScreens;
                return;
            }

            //IngredientsListView.ItemsSource = ingredients;
        }


        void SearchBar_Focused(object sender, FocusEventArgs e)
        {
            openBottomSheet();
        }


        void openBottomSheet()
        {
            var finalTranslation = Math.Max(Math.Min(0, -1000), -Math.Abs(getProportionCoordinate(.50)));
            bottomSheet.TranslateTo(bottomSheet.X, finalTranslation, 150, Xamarin.Forms.Easing.SpringIn);
        }


        public void OnPinClicked(object sender, PinClickedEventArgs e)
        {
            var span = DateTime.Now - lastClickedTime;
            if ((Int32)span.TotalMilliseconds > 1000)
            {
                var xPin = e.Pin;
                Console.WriteLine("Tapped on pin: " + xPin.Label);

                //hide all callouts
                //foreach (Pin aPin in mapView.Pins)
                //{
                //    aPin.HideCallout();
                //}

                //if (e.Pin.IsCalloutVisible())
                //    xPin.HideCallout();
                //else
                //{
                xPin.ShowCallout();
                //}
            }
            lastClickedTime = DateTime.Now;
        }



        void OnPanUpdated(object sender, PanUpdatedEventArgs e)
        {
            // Handle the pan
            switch (e.StatusType)
            {
                case GestureStatus.Running:
                    // Translate and ensure we don't y + e.TotalY pan beyond the wrapped user interface element bounds.
                    var translateY = Math.Max(Math.Min(0, y + e.TotalY), -Math.Abs((Height * .25) - Height));
                    bottomSheet.TranslateTo(bottomSheet.X, translateY, 20);
                    break;
                case GestureStatus.Completed:
                    // Store the translation applied during the pan
                    y = bottomSheet.TranslationY;

                    //at the end of the event - snap to the closest location
                    var finalTranslation = Math.Max(Math.Min(0, -1000), -Math.Abs(getClosestLockState(e.TotalY + y)));

                    //depending on Swipe Up or Down - change the snapping animation
                    if (isSwipeUp(e))
                    {
                        bottomSheet.TranslateTo(bottomSheet.X, finalTranslation, 250, Xamarin.Forms.Easing.SpringIn);
                    }
                    else
                    {
                        bottomSheet.TranslateTo(bottomSheet.X, finalTranslation, 250, Xamarin.Forms.Easing.SpringOut);
                    }

                    break;

            }

        }

        public bool isSwipeUp(PanUpdatedEventArgs e)
        {
            if (e.TotalY < 0)
            {
                return true;
            }
            return false;
        }

        //TO-DO: Make this cleaner
        public double getClosestLockState(double TranslationY)
        {
            //Play with these values to adjust the locking motions - this will change depending on the amount of content ona  apge
            var lockStates = new double[] { 0, .5, .85 };

            //get the current proportion of the sheet in relation to the screen
            var distance = Math.Abs(TranslationY);
            var currentProportion = distance / Height;

            //calculate which lockstate it's the closest to
            var smallestDistance = 10000.0;
            var closestIndex = 0;
            for (var i = 0; i < lockStates.Length; i++)
            {
                var state = lockStates[i];
                var absoluteDistance = Math.Abs(state - currentProportion);
                if (absoluteDistance < smallestDistance)
                {
                    smallestDistance = absoluteDistance;
                    closestIndex = i;
                }
            }

            var selectedLockState = lockStates[closestIndex];
            var TranslateToLockState = getProportionCoordinate(selectedLockState);

            return TranslateToLockState;
        }

        public double getProportionCoordinate(double proportion)
        {
            return proportion * Height;
        }


        private void LoadXML()
        {
            //open config XML file
            //var basePath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            //var appConfigXMLPath = Path.Combine(basePath, "appconfig.xml");

            Assembly assembly = IntrospectionExtensions.GetTypeInfo(typeof(App)).Assembly;
            Stream xmlFileStream = assembly.GetManifestResourceStream("mapsuidebug.appconfig.xml");
           
            mainXML = XDocument.Load(xmlFileStream);

            var mySerializer = new XmlSerializer(typeof(AppConfig));
            TextReader reader = new StringReader(mainXML.ToString());
            AppConfig appConfig = (AppConfig)mySerializer.Deserialize(reader);

            appScreens = new List<LangScreen>();
            string selLanguange = "L1";

            foreach (Screen appScr in appConfig.AppScreens)
            {
                LangScreen scrToAdd = new LangScreen();
                scrToAdd.ScreenId = appScr.ScreenId;
                scrToAdd.ParentId = appScr.ParentId;
                scrToAdd.ScreenTypeId = appScr.ScreenTypeId;
                if (selLanguange == "L1")
                {
                    scrToAdd.ScreenTitle = appScr.ScreenTitle;
                    scrToAdd.ScreenSubTitle = appScr.ScreenSubTitle;
                }
                scrToAdd.MapPinLat = appScr.MapPinLat;
                scrToAdd.MapPinLon = appScr.MapPinLon;
                scrToAdd.MapPinType = appScr.MapPinType;
                scrToAdd.LangScreenContentItems = new List<LangScreenContent>();

                foreach (ScreenContent appScrCont in appScr.ScreenContentItems)
                {
                    LangScreenContent scrContToAdd = new LangScreenContent();
                    scrContToAdd.ScreenContentId = appScrCont.ScreenContentId;
                    scrContToAdd.ScreenId = appScrCont.ScreenId;
                    scrContToAdd.ContentType = appScrCont.ContentType;
                    scrContToAdd.ContentOrder = appScrCont.ContentOrder;
                    scrContToAdd.ContentSubType = appScrCont.ContentSubType;
                    scrContToAdd.ContentSubTypeOrder = appScrCont.ContentSubTypeOrder;
                    if (selLanguange == "L1")
                    {
                        scrContToAdd.Title = appScrCont.Title;
                        scrContToAdd.SubTitle = appScrCont.SubTitle;
                        scrContToAdd.TextDescription = appScrCont.TextDescription;
                    }
                    scrContToAdd.Picture = appScrCont.Picture;
                    scrContToAdd.ActionType = appScrCont.ActionType;
                    scrContToAdd.ActionContent = appScrCont.ActionContent;
                    scrContToAdd.SubItemParameters = appScrCont.SubItemParameters;
                    scrToAdd.LangScreenContentItems.Add(scrContToAdd);
                }

                appScreens.Add(scrToAdd);
            }
        }

    }
}
